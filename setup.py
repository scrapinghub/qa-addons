from setuptools import setup

setup(
    name="qa_addons",
    version="1.0.0",
    description="Functions frequently used on Jupyter Notebooks we create using MATT and Pandas",
    py_modules=["qa_addons"],
)
