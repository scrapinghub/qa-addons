### What is this repository for? ###

* To host functions and act as a library for boilerplate we build in Jupyter Notebooks using MATT and Pandas

### How do I get set up? ###

1. > !pip install git+https://bitbucket.org/scrapinghub/qa-addons.git
2. > from qa_addons import *
3. And then, just use the functions on your notebook, e.g:
    > print_text("Hello World")

### Contribution guidelines ###

* Not defined, feel free to add functions and fixes as you seem fit while being cautious on not breaking notebooks in place
* Before doing the commit, in root, remember to:
* > black .

### Who do I talk to? ###

* QA: Warley, Ivan, Artur