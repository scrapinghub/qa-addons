import logging
import os
import pathlib
import re

import tldextract
from jira import JIRA


def domain_from_title(title: str):
    return ".".join(
        [tldextract.extract(title).domain, tldextract.extract(title).suffix]
    )


def get_last_job_from_issue(issue, jira_session) -> str:
    """Get the last job ID mentioned in issue comments."""
    job_list = []
    jissue = jira_session.issue(issue.key)
    comments = jissue.fields.comment.comments
    for comment in comments:
        found_jobs = re.findall(
            r"http[s]?://app\.zyte\.com/p/(\d+/\d+/\d+)", comment.body
        )
        job_list.extend(found_jobs)
    return job_list[-1] if job_list else None


def get_last_jobs_mentioned(issue, jira_session) -> str:
    """Get the last job IDs mentioned in the comments"""
    job_list = []
    jissue = jira_session.issue(issue.key)
    comments = jissue.fields.comment.comments
    for comment in reversed(comments):
        found_jobs = re.findall(
            r"http[s]?://app\.zyte\.com/p/(\d+/\d+/\d+)", comment.body
        )
        if len(found_jobs) > 0:
            job_list = list(
                set(found_jobs)
            )  # dedupe as jira returns duplicated whenever hyperlinks an URL
            break
    return job_list


def get_ready_to_qa_issues(
    jira_session, project_key: str, status: str = "Ready to QA"
) -> list:
    ready_to_qa_issues = []
    i = 0
    chunk_size = 100
    while True:
        chunk = jira_session.search_issues(
            f'project = {project_key} AND status = "{status}"',
            startAt=i,
            maxResults=chunk_size,
        )
        i += chunk_size
        ready_to_qa_issues += chunk.iterable
        if i >= chunk.total:
            break
    return ready_to_qa_issues


def get_current_issues_to_scan(
    jira_session, project_key: str, status: str = "Ready to QA"
):
    issue_dict = {}
    ready_to_qa_issues = get_ready_to_qa_issues(jira_session, project_key, status)
    for issue in ready_to_qa_issues:
        issue_dict[domain_from_title(issue.fields.summary)] = {
            "issue_key": issue.key,
            "job": get_last_job_from_issue(issue, jira_session),
        }
    if issue_dict:
        logging.info(f"Found issues to scan: {issue_dict}")
    else:
        logging.info(f"No issues to scan.")
    return issue_dict


def reassign_ticket(jira_issue_key: str, transition_status: str = "21"):
    jira = get_jira_session()
    jira.transition_issue(
        jira_issue_key,
        transition_status,  # "21" = "In Progress" status key, ID or status name can be used
    )
    author = [
        comment
        for comment in jira.comments(jira_issue_key)
        if re.findall(r"http[s]?://app\.zyte\.com/p/(\d+/\d+/\d+)", comment.body)
    ][
        -1
    ].author  # Author of the spider - author of the last comment with job link.
    jira.issue(jira_issue_key).update(
        fields={"assignee": {"accountId": author.accountId}}
    )


def attach_report_to_ticket(jira_issue_key: str, report_path: pathlib.Path):
    message = """The attached report contains the result of the scan of comparison of some items from last crawl with data extracted from the page. 

    Please check the report for possible issues, fix them and run the job again. Please report any false positives if you can.

    This message is generated automatically.

    [^{report_path.name}]
    """
    jira = get_jira_session()
    jira.add_attachment(issue=jira_issue_key, attachment=report_path.as_posix())
    jira.add_comment(jira_issue_key, message)
    logging.info(f"Report uploaded for issue {jira_issue_key}.")


def label_ticket(jira_issue_key: str, label: str = "scanned"):
    jira = get_jira_session()
    jira_issue = jira.issue(jira_issue_key)
    jira_issue.fields.labels.append(label)
    jira_issue.update(fields={"labels": jira_issue.fields.labels})
    logging.info(f'Issue {jira_issue_key} labelled as "{label}".')


def get_jira_session():
    """JIRA_API_TOKEN and JIRA_EMAIL must be defined as env variables."""
    jira_session = JIRA(
        {"server": "https://zyte.atlassian.net/"},
        basic_auth=(os.environ.get("JIRA_EMAIL"), os.environ.get("JIRA_API_TOKEN")),
    )
    return jira_session
