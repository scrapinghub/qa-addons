import random
import re
from urllib.parse import urljoin

import numpy
import pandas as pd
import parsel
import requests
from IPython.core.display import display, HTML, Markdown
from scrapinghub import ScrapinghubClient
from tqdm.notebook import tqdm
from url_summary import url_summary
import warnings

def core_2_converter(df):
    display("Converting items...")
    df_core1 = (
        pd.DataFrame(df["extractorResults"].tolist())[0]
        .apply(pd.Series)["products"]
        .apply(pd.Series)
        .T
    )
    ddd_t = []

    for col in tqdm(df_core1.columns):
        df_temp = pd.DataFrame(df_core1[col].dropna().apply(pd.Series))
        df_temp["item"] = col
        ddd_t.append(df_temp)

    df_core2 = pd.DataFrame(pd.concat(ddd_t))
    return df_core2


def get_items(job_id, flatten=False):
    warnings.filterwarnings("ignore", "This pattern has match groups")
    pd.options.display.max_colwidth = 120
    pd.options.display.precision = 3
    display(HTML("<style>.container { width:80% !important; }</style>"))
    display("Retrieving items...")
    items = [
        item
        for item in tqdm(ScrapinghubClient().get_job(job_id).items.iter(meta=["_key"]))
    ]
    items_df = pd.DataFrame(items)

    if flatten:
        # break down additionalProperty into columns
        if "additionalProperty" in items_df.columns:
            items_df = pd.concat(
                [
                    items_df,
                    pd.DataFrame(
                        [
                            {f"additionalProperty.{v['name']}": v["value"] for v in row}
                            for row in items_df[
                                items_df.additionalProperty.notna()
                            ].additionalProperty
                        ]
                    ),
                ],
                axis=1,
            )

    df = items_df.drop(columns=["_type"])

    v_df = None
    variants = None
    if "variants" in df.columns:
        v_df = pd.DataFrame(
            [variant for variant in df["variants"] if type(variant) == list]
        )
        v_df = v_df.melt(id_vars=None).drop(columns="variable")
        variants = list(v_df["value"].dropna().values)
        v_df = pd.DataFrame(variants)

    df_core2 = core_2_converter(df)

    return items, df, variants, v_df, df_core2


def color_negative_red(val):
    """
    Takes a scalar and returns a string with
    the css property `'color: red'` for negative
    strings, black otherwise.
    """
    if str(val) == "nan":
        return ""
    else:
        color = "lightgreen" if val == "in stock" else "yellow"
        return "background-color: %s" % color


def get_dash_link_from_key(_key):
    _key = str(_key)
    last_slash_position = _key.rindex("/")
    dash_link_to_item = (
        "https://app.zyte.com/p/"
        + _key[:last_slash_position]
        + "/item"
        + _key[last_slash_position:]
    )
    return dash_link_to_item


def get_logs(job_id):
    job_logs = []

    job = ScrapinghubClient().get_job(job_id)
    url = job.metadata.get("spider_args")
    display("Retrieving logs...")
    for logs in tqdm(job.logs.list()):
        job_logs.append({"job_id": job_id, "request": logs, "url": url})
    df_logs = pd.DataFrame(job_logs)
    df_logs = df_logs.request.apply(pd.Series)
    # display("Logs above level 20:")
    # display(df_logs[df_logs.level > 20].message)
    return df_logs


def get_samples(df, amount):
    size = len(df.index)
    if size >= amount:
        samples = df.sample(amount)
    else:
        samples = df.sample(size)
    return samples


def display_sample_data(df):
    list_of_output = []
    display("Displaying samples...")
    # generic
    for column in tqdm(df.columns):
        sample = df[column].value_counts(2).head(15)
        if (
            column == "item"
            or column == "sku==id"
            or column == "description_structured"
            or column == "description"
            or column == "image_links"
            or column == "key_value_pairs"
            or column == "options"
            or column == "bullets"
            or column == "link"
        ):
            continue
        elif "[{" not in str(sample):
            list_of_output.append(
                HTML(
                    "<center><h1>Product-level Data: " + column + "</h1></center><hr> "
                )
            )
            list_of_output.append(sample.to_frame())
            column_type = type(df[df[column].notna()][column].values[0])
            if column_type is not list and column_type is not dict:
                unique_list = df[column].unique()
                if len(unique_list) != int(df.shape[0]):
                    list_of_output.append(HTML("15 Unique: "))
                    list_of_output.append(unique_list[:15])
            converted = pd.to_numeric(df[column], errors="ignore")
            if "int" in str(converted.dtype) or "float" in str(converted.dtype):
                list_of_output.append(converted.plot(kind="bar", figsize=(30, 8)))
                temp_df = df[[column]].copy(deep=True)
                temp_df[column] = converted
                temp_df["z_score"] = (
                    (temp_df[column] - temp_df[column].mean()) / temp_df[column].std()
                ).abs()
                list_of_output.append(
                    HTML("<center><h1>Highest & Lowest Values:</h1></center>")
                )
                list_of_output.append(
                    temp_df.sort_values(by="z_score", ascending=False).head(3)
                )
                list_of_output.append(
                    temp_df.sort_values(by="z_score", ascending=False).tail(3)
                )
            list_of_output.append(HTML("<center><h1>NA() Cases:</h1></center>"))
            list_of_output.append(get_samples(df[df[column].isna()], 2)[[column]])
            list_of_output.append(HTML("<center><h1>Random Check:</h1></center>"))
            list_of_output.append(get_samples(df[df[column].notna()], 2)[[column]])
        else:
            df_column = df[df[column].notna()]
            df_column_list = [
                df_column[column][i] for i in list(df_column[column].index)
            ]
            df_flattened_column_list = [
                item for sublist in df_column_list for item in sublist
            ]
            df_from_nested = pd.DataFrame(df_flattened_column_list)
            for column_from_nested in df_from_nested.columns:
                list_of_output.append(
                    HTML("<center><h1>Variant-level Data:</h1></center>")
                )
                list_of_output.append(
                    df_from_nested[column_from_nested].value_counts(2).to_frame()
                )
                list_of_output.append(
                    get_samples(
                        df_from_nested[df_from_nested[column_from_nested].notna()], 2
                    )[column_from_nested]
                )
                column_type = type(df[df[column].notna()][column].values[0])
                if column_type is not list and column_type is not dict:
                    unique_list = df_from_nested[column_from_nested].unique()
                    if len(unique_list) != int(df.shape[0]):
                        list_of_output.append(HTML("15 Unique: "))
                        list_of_output.append(unique_list[:15])
            if "breadcrumbs" in df_from_nested.columns:
                list_of_output.append(
                    HTML("<center><h1>Variant-level Data:</h1></center>")
                )
                list_of_output.append(
                    df_from_nested.breadcrumbs.apply(pd.Series)[0]
                    .apply(pd.Series)["name"]
                    .value_counts()
                    .head(10)
                )
                list_of_output.append(df_from_nested.breadcrumbs.apply(pd.Series))

    # generic
    display(HTML("<center><h1>Overall Data Summary</h1></center>"))
    print("Items: %s\nFields: %s" % df.shape)
    display(df.describe().T)
    [display(output) for output in list_of_output]
    sample_items = df.sample(10).copy(deep=True)
    for url in sample_items.link.values:
        print(url)
    display(sample_items.T)

    # specific
    if "breadcrumbs" in df.columns:
        display(df.breadcrumbs.apply(pd.Series)[0].apply(pd.Series)[0].value_counts())
        display(df.breadcrumbs.apply(pd.Series))
    else:
        print("breadcrumbs not within fields")
    if "description_structured" in df.columns:
        display(HTML("<center><h1>description_structured" + "</h1></center><hr> "))
        display(df[df["description_structured"].notna()]["link"].iloc[0])
        display(HTML("<hr><br>"))
        display(
            HTML(
                df[df["description_structured"].notna()]["description_structured"].iloc[
                    0
                ]["sections"][0]["content"]
            )
        )
        list_of_output.append(url_summary.get_summary(df["link"]))
    else:
        print("description_structured not within fields")


def sitemap_check(sitemap, base_product_url, df):
    try:
        if (
            sitemap
            and base_product_url
            and sitemap != ""
            and base_product_url != ""
            and isinstance(sitemap, str)
            and isinstance(base_product_url, str)
        ):  # improve https://github.com/scrapinghub/item-coverage-playbook/blob/master/scripts/

            display(
                HTML(
                    '<div style="background-color:LightGray;text-align:center;">'
                    "Sitemap Checking with Items Scraped"
                    "</div>"
                )
            )

            main_sitemap = requests.get(
                sitemap
            )  # paste base sitemap link, e.g. "https://shoprumours.com/sitemap.xml"
            product_base_url = base_product_url  # paste base product link, e.g. "https://shoprumours.com/products/"

            product_slugs = []
            sitemaps = parsel.Selector(main_sitemap.text).css("loc::text").getall()

            if len(sitemaps) > 0:
                display("Processing sitemaps...")
                for sitemap in tqdm(sitemaps):
                    if "_products" in sitemap:
                        products_sitemap = requests.get(sitemap)
                        product_slugs.extend(
                            parsel.Selector(products_sitemap.text)
                            .css("url>loc::text")
                            .re("products/(.+)")
                        )
                missing_product_urls = [
                    urljoin(product_base_url, product_slug)
                    for product_slug in product_slugs
                    if product_slug
                    not in set(df.url.str.extract("/([^/]+)$").values.flatten())
                ]
                if len(product_slugs) == 0:
                    display(
                        f"The script didn't find any product in the sitemap:\n{sitemaps}"
                    )
                else:
                    display(f"Found {len(missing_product_urls)} missing product urls.")
                    for product_url in missing_product_urls:
                        display(product_url)
            else:
                display(
                    f"The script couldn't process the sitemap, the content was as below:\n{main_sitemap.text}"
                )
        else:
            display(f"Skipping sitemap check as expected variables weren't filled.")
    except Exception as e:
        display(f"Sitemap check script failed: {e}")


def process_df_test(t_df, message, sample_amount=3, manual=False):
    count = t_df.shape[0]
    t_count = t_df.shape[0]
    precision_above_98 = True
    msg = message + f": {count}"
    samples = None
    if count > 0:
        try:
            samples = t_df.sample(sample_amount)
        except ValueError:
            if len(t_df.index) > 0:
                samples = t_df.sample(len(t_df.index))
        if "link" in samples.columns:
            samples["link"] = samples.apply(
                lambda x: "Variant-Level: " + apply_anchor(x["link"]), axis=1
            )
        if "pageUrl" in samples.columns:
            samples["pageUrl"] = samples.apply(
                lambda x: "Product-Level: " + apply_anchor(x["pageUrl"]), axis=1
            )
        if "_key" in samples.columns:
            samples["_key"] = samples.apply(
                lambda x: apply_anchor(get_dash_link_from_key(x["_key"])), axis=1
            )
            samples = samples[["_key", "link"]].to_string(index=False, header=False)
        elif "link" in samples.columns:
            samples = samples[["link"]].to_string(index=False, header=False)
        else:
            samples = samples.to_string(index=False, header=False)
        msg += f", e.g: {samples}"
        precision_above_98 = (count / t_count) <= 0.02
    if precision_above_98:
        msg = msg.replace("<b>", "").replace("</b>", "")
    if manual:
        return None, msg
    else:
        return precision_above_98, msg


def apply_anchor(url):
    if "<a href" not in url:
        if "app.zyte" in url:
            return f'<a href="{url}" target="_blank">Dash Item</a> '
        else:
            return f'<a href="{url}" target="_blank">URL</a> '
    else:
        return url


def get_dash_link_from_key_without_anchor(_key):
    if "https://app.zyte.com/p/" not in _key:
        if "/item/" in _key:
            _key.replace("/item/", "")
        _key = str(_key)
        last_slash_position = _key.rindex("/")
        _key = (
            "https://app.zyte.com/p/"
            + _key[:last_slash_position]
            + "/item"
            + _key[last_slash_position:]
        )
    return _key


def clean_url(url):
    c_url = url
    if "/products/" in c_url and (".png" not in c_url and ".jpg" not in c_url):
        search = re.search(r".*/products(\S+)(?:\?.+=.+)?(?:'+.*)?", c_url)
        if search and len(search.groups()) > 0:
            c_url = search.group(1)
            c_url_more = re.search(r"^(/.+)'", c_url)
            c_url = (
                c_url_more.group(1)
                if c_url_more and len(c_url_more.groups()) > 0
                else c_url
            )
            c_url_more = re.search(r"(/\S+)(?:\?.+=.+)+", c_url)
            c_url = (
                c_url_more.group(1)
                if c_url_more and len(c_url_more.groups()) > 0
                else c_url
            )
    else:
        search = re.search(
            r"Dropped: Duplicated item(?:.|\n)+?(https\S+)'(?:.|\n)+?'id': '(.+)'", url
        )
        if search and len(search.groups()) > 0:
            c_url = search.group(1)
            c_url = c_url[c_url.rindex("/") :]
    return c_url


def check_incorrectly_dropped_duplicate_urls(job_id, df):
    display(
        HTML(
            '<div style="background-color:LightGray;text-align:center;">'
            "Checking Possibly Incorrect Deduplication (URL)"
            "</div>"
        )
    )
    df_logs = get_logs(job_id)
    duplicates_dropped = df_logs[
        df_logs["message"].str.contains("Dropped: Duplicated item")
    ][["message"]]
    duplicates_dropped["message"] = duplicates_dropped.apply(
        lambda x: clean_url(x["message"]), axis=1
    )
    possibly_missing = duplicates_dropped.drop_duplicates()
    possibly_missing = possibly_missing[possibly_missing["message"].notna()][
        "message"
    ].values
    print(f"Unique dropped URLs:{len(possibly_missing)}")

    df["clean_url"] = df.apply(lambda x: clean_url(x["pageUrl"]), axis=1)
    job_urls = df["clean_url"].unique()
    print(f"Unique item URLs in job:{len(job_urls)}")

    possibly_incorrectly_urls_dropped = []
    if len(possibly_missing) > 0:
        possibly_incorrectly_urls_dropped = numpy.setdiff1d(possibly_missing, job_urls)
    print(
        f"Amount possibly missing from dropped:{len(possibly_incorrectly_urls_dropped)}"
    )
    display(possibly_incorrectly_urls_dropped)


def get_id_from_log(log):
    search = re.search(
        r"Dropped: Duplicated item(?:.|\n)+?(https\S+)'(?:.|\n)+?'id': '(\S+)'", log
    )
    if search and len(search.groups()) > 1:
        return search.group(2)
    else:
        return None


def check_incorrectly_dropped_duplicate_ids(job_id, df):
    display(
        HTML(
            '<div style="background-color:LightGray;text-align:center;">'
            "Checking Possibly Incorrect Deduplication (ID)"
            "</div>"
        )
    )
    df_logs = get_logs(job_id)
    duplicates_dropped = df_logs[
        df_logs["message"].str.contains("Dropped: Duplicated item")
    ][["message"]]
    duplicates_dropped["message"] = duplicates_dropped.apply(
        lambda x: get_id_from_log(x["message"]), axis=1
    )
    possibly_missing = duplicates_dropped.drop_duplicates()
    possibly_missing = possibly_missing[possibly_missing["message"].notna()][
        "message"
    ].values
    print(f"Unique dropped IDs:{len(possibly_missing)}")

    job_ids = df["id"].drop_duplicates().values
    print(f"Unique item IDs in job(plus variants):{len(job_ids)}")

    possibly_incorrectly_ids_dropped = numpy.setdiff1d(possibly_missing, job_ids)
    print(
        f"Amount possibly missing from dropped:{len(possibly_incorrectly_ids_dropped)}"
    )
    display(possibly_incorrectly_ids_dropped)


def show_test_results(tests):
    tests_result = ""
    display(
        HTML(
            '<div style="background-color:LightGray;text-align:center;">Automated Tests Suite</div>'
        )
    )
    for test in tests:
        if test[0] is True:
            tests_result += f'<div style="background-color:DarkSeaGreen;">Passed: {test[1]}</div><br>'
        elif test[0] is False:
            tests_result += (
                f'<div style="background-color:LightCoral;">Failed: {test[1]}</div><br>'
            )
        elif test[0] is None:
            tests_result += f'<div style="background-color:LightGray;">Manually check: {test[1]}</div><br>'
    display(HTML(tests_result))


def get_library_tests(df, df_core2):
    tests = []

    # Checks for a specific pattern of SKU
    t_msg = f"Occurrences of <b>item level SKUs containing variant SKU</b>"
    try:
        t_df = df_core2[df_core2["sku"].str.contains("/[A-Z]{3}_[A-Z]")]
        tests.append(process_df_test(t_df, t_msg))
    except Exception as e:
        tests.append(
            [
                None,
                f"{t_msg.replace('<b>', '').replace('</b>', '')}: Test could not be executed due to: {repr(e)}",
            ]
        )

    # Top level / item URL containing variant relative path / parameters
    t_msg = f"Occurrences of <b>item URLs containing variants or parameters</b>"
    try:
        t_df = df_core2[df_core2["link"].str.contains(r"\?.+=.+")]
        tests.append(process_df_test(t_df, t_msg))
    except Exception as e:
        tests.append(
            [
                None,
                f"{t_msg.replace('<b>', '').replace('</b>', '')}: Test could not be executed due to: {repr(e)}",
            ]
        )

    # cases of price > regularPrice in item level
    t_msg = f"Occurrences of <b>price > regularPrice in item level</b>"
    if "regularPrice_int" in df.columns and "price_int" in df.columns:
        try:
            t_df = df[df["regularPrice_int"].notna()]
            t_df = t_df[t_df["price_int"].notna()]
            t_df = t_df[t_df["price_int"] > t_df["regularPrice_int"]]
            tests.append(process_df_test(t_df, t_msg))
        except Exception as e:
            tests.append(
                [None, f"{t_msg}: Test could not be executed due to: {repr(e)}"]
            )
    else:
        # TODO: convert each regularPrice and price above from str into int and check others tests too
        t_df = None  # place holder

    # cases of regularPrice == price in item level by int
    t_msg = f"Occurrences of <b>regularPrice == price in item level checked by int conversion</b>"
    if "regularPrice_int" in df.columns and "price_int" in df.columns:
        try:
            t_df = df[df["regularPrice_int"].notna()]
            t_df = t_df[t_df["price_int"].notna()]
            t_df = t_df[t_df["regularPrice_int"] == t_df["price_int"]]
            tests.append(process_df_test(t_df, t_msg))
        except Exception as e:
            tests.append(
                [None, f"{t_msg}: Test could not be executed due to: {repr(e)}"]
            )

    # cases of regularPrice == price in item level by string
    t_msg = f"Occurrences of <b>regularPrice == price in item level checked by string comparison</b>"
    if "regularPrice" in df.columns and "price" in df.columns:
        try:
            t_df = df[df["regularPrice"].notna()]
            t_df = t_df[t_df["price"].notna()]
            t_df = t_df[t_df["regularPrice"] == t_df["price"]]
            tests.append(process_df_test(t_df, t_msg))
        except Exception as e:
            tests.append(
                [None, f"{t_msg}: Test could not be executed due to: {repr(e)}"]
            )

    # cases of price > regularPrice variants level
    t_msg = f"Occurrences of <b>price > regularPrice in variants level</b>"
    if "regularPrice_int" in df_core2.columns and "price_int" in df_core2.columns:
        try:
            t_df = df_core2[df_core2["regularPrice_int"].notna()]
            t_df = t_df[t_df["price_int"].notna()]
            t_df = t_df[t_df["price"] > t_df["regularPrice_int"]]
            tests.append(process_df_test(t_df, t_msg))
        except Exception as e:
            tests.append(
                [None, f"{t_msg}: Test could not be executed due to: {repr(e)}"]
            )

    # cases of regularPrice == price in variants level by int
    t_msg = f"Occurrences of <b>regularPrice == price in variants level checked by int conversion</b>"
    if "regularPrice_int" in df_core2.columns and "price_int" in df_core2.columns:
        try:
            t_df = df_core2[df_core2["regularPrice_int"].notna()]
            t_df = t_df[t_df["price_int"].notna()]
            t_df = t_df[t_df["regularPrice_int"] == t_df["price_int"]]
            tests.append(process_df_test(t_df, t_msg))
        except Exception as e:
            tests.append(
                [None, f"{t_msg}: Test could not be executed due to: {repr(e)}"]
            )

    # cases of regularPrice == price in variants level by string
    t_msg = f"Occurrences of <b>regularPrice == price in variants level checked by string comparison</b>"
    if "regularPrice" in df_core2.columns and "price" in df_core2.columns:
        try:
            t_df = df_core2[df_core2["regularPrice"].notna()]
            t_df = t_df[t_df["price"].notna()]
            t_df = t_df[t_df["regularPrice"] == t_df["price"]]
            tests.append(process_df_test(t_df, t_msg))
        except Exception as e:
            tests.append(
                [None, f"{t_msg}: Test could not be executed due to: {repr(e)}"]
            )

    # SKU scraped from IDs
    t_msg = f"Occurrences of <b>item SKUs containing the same as IDs</b>"
    if "id" in df.columns and "sku" in df.columns:
        try:
            t_df = df
            t_df["sku==id"] = t_df.apply(lambda x: x["id"] == x["sku"], axis=1)
            t_df = t_df[t_df["sku==id"]]
            tests.append(process_df_test(t_df, t_msg))
        except Exception as e:
            tests.append(
                [None, f"{t_msg}: Test could not be executed due to: {repr(e)}"]
            )

    # SKU scraped from IDs on variants
    t_msg = f"Occurrences of <b>variant SKUs containing the same as IDs</b>"
    if "id" in df_core2.columns and "sku" in df_core2.columns:
        try:
            t_df = df_core2
            t_df["sku==id"] = t_df.apply(lambda x: x["id"] == x["sku"], axis=1)
            t_df = t_df[t_df["sku==id"]]
            tests.append(process_df_test(t_df, t_msg))
        except Exception as e:
            tests.append(
                [None, f"{t_msg}: Test could not be executed due to: {repr(e)}"]
            )

    # Duplicated IDs
    t_msg = f"Occurrences of <b>duplicated item IDs</b>"
    try:
        t_df = df[df["id"].duplicated()]
        tests.append(process_df_test(t_df, t_msg))
    except Exception as e:
        tests.append(
            [
                None,
                f"{t_msg.replace('<b>', '').replace('</b>', '')}: Test could not be executed due to: {repr(e)}",
            ]
        )

    # Duplicated SKUs
    if "sku" in df.columns:
        try:
            t_df = df[df["sku"].notna()]
            t_df = t_df[t_df["sku"].duplicated()]
            t_msg = f"Occurrences of <b>duplicated item SKUs</b>"
            tests.append(process_df_test(t_df, t_msg))
        except Exception as e:
            tests.append(
                [None, f"{t_msg}: Test could not be executed due to: {repr(e)}"]
            )

    # Duplicated URLs
    t_msg = f"Occurrences of <b>duplicated item URLs</b>"
    try:
        t_df = df[df["url"].duplicated()]
        tests.append(process_df_test(t_df, t_msg))
    except Exception as e:
        tests.append(
            [
                None,
                f"{t_msg.replace('<b>', '').replace('</b>', '')}: Test could not be executed due to: {repr(e)}",
            ]
        )

    # Duplicated variants per URL
    t_msg = f"Occurrences of <b>duplicated variants based on their URL</b>"
    try:
        t_df = df_core2[df_core2["url"].duplicated()]
        tests.append(process_df_test(t_df, t_msg))
    except Exception as e:
        tests.append(
            [
                None,
                f"{t_msg.replace('<b>', '').replace('</b>', '')}: Test could not be executed due to: {repr(e)}",
            ]
        )

    # Duplicated variants per ID
    t_msg = f"Occurrences of <b>duplicated variants based on their ID</b>"
    try:
        t_df = df_core2[df_core2["id"].duplicated()]
        tests.append(process_df_test(t_df, t_msg))
    except Exception as e:
        tests.append(
            [
                None,
                f"{t_msg.replace('<b>', '').replace('</b>', '')}: Test could not be executed due to: {repr(e)}",
            ]
        )

    # Duplicated variants per SKU
    t_msg = f"Occurrences of <b>duplicated variants based on their SKU</b>"
    try:
        t_df = df_core2
        t_df = t_df[t_df["sku"].notna()]
        t_df = t_df[t_df["sku"].duplicated()]
        tests.append(process_df_test(t_df, t_msg, manual=True))
    except Exception as e:
        tests.append(
            [
                None,
                f"{t_msg.replace('<b>', '').replace('</b>', '')}: Test could not be executed due to: {repr(e)}",
            ]
        )

    # Cases of variants without SKU
    t_msg = f"Occurrences of <b>variants without SKU</b>"
    try:
        t_df = df_core2[df_core2["sku"].isna()]
        tests.append(process_df_test(t_df, t_msg, manual=True))
    except Exception as e:
        tests.append(
            [
                None,
                f"{t_msg.replace('<b>', '').replace('</b>', '')}: Test could not be executed due to: {repr(e)}",
            ]
        )

    # # regularPrice shouldn"t have currency inside like USD 10.0 pattern
    t_msg = (
        f"Occurrences of <b>price containing currency inside USD 10.0 in item level</b>"
    )
    if "price" in df.columns:
        try:
            t_df = df[df["price"].astype("string").str.contains("USD ", na=False)]
            tests.append(process_df_test(t_df, t_msg))
        except Exception as e:
            tests.append(
                [None, f"{t_msg}: Test could not be executed due to: {repr(e)}"]
            )

    # regularPrice shouldn"t have currency inside like USD 10.0 pattern
    t_msg = f"Occurrences of <b>regularPrice containing currency inside USD 10.0 in item level</b>"
    if "regularPrice" in df.columns:
        try:
            t_df = df[
                df["regularPrice"].astype("string").str.contains("USD ", na=False)
            ]
            tests.append(process_df_test(t_df, t_msg))
        except Exception as e:
            tests.append(
                [None, f"{t_msg}: Test could not be executed due to: {repr(e)}"]
            )

    # # regularPrice shouldn"t have currency inside like USD 10.0 pattern in variants
    t_msg = f"Occurrences of <b>price containing currency inside USD 10.0 in variants level</b>"
    if "price" in df_core2.columns:
        try:
            t_df = df_core2[
                df_core2["price"].astype(str).str.contains("USD ", na=False)
            ]
            tests.append(process_df_test(t_df, t_msg))
        except Exception as e:
            tests.append(
                [None, f"{t_msg}: Test could not be executed due to: {repr(e)}"]
            )

    # regularPrice shouldn"t have currency inside like USD 10.0 pattern in variants
    t_msg = f"Occurrences of <b>regularPrice containing currency inside USD 10.0 in variants level</b>"
    if "regularPrice" in df_core2.columns:
        try:
            t_df = df_core2[df_core2["regularPrice"].str.contains("USD ", na=False)]
            tests.append(process_df_test(t_df, t_msg))
        except Exception as e:
            tests.append(
                [None, f"{t_msg}: Test could not be executed due to: {repr(e)}"]
            )

    # Cases of variants without relative paths
    t_msg = f"Occurrences of <b>variants without relative paths</b>"
    try:
        t_df = df_core2[~df_core2["url"].str.contains(r"\?.+=.+")]
        samples = [apply_anchor(url) for url in t_df["url"]]
        samples = random.sample(samples, 3) if len(samples) >= 3 else samples
        t_msg = (
            t_msg.replace("<b>", "").replace("</b>", "")
            if t_df.shape[0] == 0 or (t_df.shape[0] / t_df.shape[0]) <= 0.02
            else t_msg
        )
        tests.append(
            [
                None,
                t_msg
                + f": {t_df.shape[0]}. Above 0 is not always a fail indication as not all "
                f"websites have relative paths to variants, e.g: {samples}",
            ]
        )
    except Exception as e:
        tests.append(
            [
                None,
                f"{t_msg.replace('<b>', '').replace('</b>', '')}: Test could not be executed due to: {repr(e)}",
            ]
        )

    # Possibly cases of invalid SKU on item
    if "sku" in df.columns:
        try:
            t_df = df[df["sku"].str.contains("^([0-9]{2}/){2}([0-9]{2})$", na=False)]
            t_msg = f"Occurrences of <b>possibly cases of invalid SKU in item level</b>"
            samples = [apply_anchor(url) for url in t_df["url"]]
            samples = random.sample(samples, 3) if len(samples) >= 3 else samples
            tests.append(process_df_test(t_df, f"{t_msg}, e.g: {samples}"))
        except Exception as e:
            tests.append([None, f"{t_msg}: Test failed due to {e}"])
    else:
        tests.append([None, "No sku field in top item level / main product level"])

    # Possibly cases of invalid SKU on variants
    t_msg = f"Occurrences of <b>possibly cases of invalid SKU in variants level</b>"
    try:
        t_df = df_core2
        t_df = t_df[t_df["sku"].notna()]
        t_df = t_df[
            t_df["sku"].astype(str).str.contains("^([0-9]{2}/){2}([0-9]{2})$", na=False)
        ]
        samples = [apply_anchor(url) for url in t_df["url"]]
        samples = random.sample(samples, 3) if len(samples) >= 3 else samples
        tests.append(process_df_test(t_df, f"{t_msg}, e.g: {samples}"))
    except Exception as e:
        tests.append(
            [
                None,
                f"{t_msg.replace('<b>', '').replace('</b>', '')}: Test could not be executed due to: {repr(e)}",
            ]
        )

    # possible low quality color picking (icon) image in variants that perhaps could be dropped
    t_msg = (
        f"Occurrences of <b>possibly low quality "
        f"color picking (icon) image in items that perhaps could be dropped</b>"
    )
    try:
        t_df = df[
            df["image_urls"].str.contains(
                "[A-z0-9]{8}-(?:[A-z0-9]{4}-){3}[A-z0-9]{12}", na=False
            )
        ]
        samples = [apply_anchor(url) for url in t_df["url"]]
        samples = random.sample(samples, 3) if len(samples) >= 3 else samples
        t_msg = (
            t_msg.replace("<b>", "").replace("</b>", "")
            if t_df.shape[0] == 0 or (df.shape[0] / t_df.shape[0]) <= 0.02
            else t_msg
        )
        tests.append([None, t_msg + f": {t_df.shape[0]}, e.g: {samples}"])
    except Exception as e:
        tests.append(
            [
                None,
                f"{t_msg.replace('<b>', '').replace('</b>', '')}: Test could not be executed due to: {repr(e)}",
            ]
        )

    # possible low quality color picking (icon) image in variants that perhaps could be dropped
    t_msg = (
        f"Occurrences of <b>possibly low quality color"
        f" picking (icon) image in variants that perhaps could be dropped</b>"
    )
    try:
        t_df = df_core2[
            df_core2["image_urls"].str.contains(
                "[A-z0-9]{8}-(?:[A-z0-9]{4}-){3}[A-z0-9]{12}", na=False
            )
        ]
        samples = [apply_anchor(url) for url in t_df["url"]]
        samples = random.sample(samples, 3) if len(samples) >= 3 else samples
        t_msg = (
            t_msg.replace("<b>", "").replace("</b>", "")
            if t_df.shape[0] == 0 or (df.shape[0] / t_df.shape[0]) <= 0.02
            else t_msg
        )
        tests.append([None, t_msg + f": {t_df.shape[0]}, e.g: {samples}"])
    except Exception as e:
        tests.append(
            [
                None,
                f"{t_msg.replace('<b>', '').replace('</b>', '')}: Test could not be executed due to: {repr(e)}",
            ]
        )

    # Cases of missing /products?/ or /shop/ in item URLs
    t_msg = f"Occurrences of <b>missing /products?/ or /shop/ in item URLs</b>"
    try:
        t_df = df_core2[
            df_core2["link"].str.contains("^((?!(/products?/)|(/shop/)).)*$")
        ][["_key", "link"]]
        tests.append(process_df_test(t_df, t_msg, manual=True))
    except Exception as e:
        tests.append(
            [
                None,
                f"{t_msg.replace('<b>', '').replace('</b>', '')}: Test could not be executed due to: {repr(e)}",
            ]
        )

    # Cases of missing /products?/ or /shop/ in variant URLs
    try:
        t_msg = f"Occurrences of <b>missing /products?/ or /shop/ in variant URLs</b>"
        t_df = df_core2[
            df_core2["link"].str.contains("^((?!(/products?/)|(/shop/)).)*$")
        ]
        tests.append(process_df_test(t_df, t_msg, manual=True))
    except Exception as e:
        tests.append(
            [
                None,
                f"{t_msg.replace('<b>', '').replace('</b>', '')}: Test could not be executed due to: {repr(e)}",
            ]
        )

    # fields containing garbage / encoded symbols
    for column in numpy.setdiff1d(
        df.select_dtypes("object").columns, ["_key", "url", "image_urls", "description"]
    ):
        t_msg = (
            f"Occurrences of field <b>{column}</b> "
            f"containing garbage / encoded symbols [\\n\\xad\\xa0]|(&[A-z0-9]+;)"
        )
        try:
            t_df = df[df[column].str.contains(r"[\n\xad\xa0]|(&[A-z0-9]+;)", na=False)]
            tests.append(process_df_test(t_df, t_msg))
        except Exception as e:
            tests.append(
                [None, f"{t_msg}: Test could not be executed due to: {repr(e)}"]
            )

    # Cases of variants containing garbage / encoded symbols
    for column in numpy.setdiff1d(
        df_core2.select_dtypes("object").columns,
        ["_key", "link", "image_urls", "description"],
    ):
        try:
            t_msg = (
                f"Occurrences of variant field <b>{column}</b>"
                f" containing garbage / encoded symbols [\\n\\xad\\xa0]|(&[A-z0-9]+;)"
            )
            t_df = df_core2[
                df_core2[column].str.contains(r"[\n\xad\xa0]|(&[A-z0-9]+;)", na=False)
            ]
            tests.append(process_df_test(t_df, t_msg))
        except Exception as e:
            tests.append(
                [None, f"{t_msg}: Test could not be executed due to: {repr(e)}"]
            )

    # description field containing garbage / encoded symbols
    t_msg = (
        f"Occurrences of field <b>description</b> "
        f"containing garbage / encoded symbols [\\n\\xad\\xa0]|(&[A-z0-9]+;)"
    )
    try:
        t_df = df[df["description"].str.contains(r"[\xad\xa0]|(&[A-z0-9]+;)", na=False)]
        tests.append(process_df_test(t_df, t_msg))
    except Exception as e:
        tests.append(
            [
                None,
                f"{t_msg.replace('<b>', '').replace('</b>', '')}: Test could not be executed due to: {repr(e)}",
            ]
        )

    # description inside variants containing garbage / encoded symbols
    t_msg = (
        f"Occurrences of variant field <b>description</b> "
        f"containing garbage / encoded symbols [\\n\\xad\\xa0]|(&[A-z0-9]+;)"
    )
    try:
        t_df = df_core2[
            df_core2["description"].str.contains(r"[\xad\xa0]|(&[A-z0-9]+;)", na=False)
        ]
        tests.append(process_df_test(t_df, t_msg))
    except Exception as e:
        tests.append(
            [
                None,
                f"{t_msg.replace('<b>', '').replace('</b>', '')}: Test could not be executed due to: {repr(e)}",
            ]
        )

    # duplicated variants level images
    t_msg = f"Occurrences of <b>duplicated variants level images</b>"
    try:
        i_df = pd.DataFrame(
            [item for sublist in df_core2["image_urls"].values for item in sublist]
        )
        i_df[0] = i_df.apply(lambda x: re.sub(r"\?\S+=\S+", "", x[0]), axis=1)
        t_df = i_df[i_df[0].duplicated()]
        tests.append(process_df_test(t_df, t_msg, manual=True))
    except Exception as e:
        tests.append(
            [
                None,
                f"{t_msg.replace('<b>', '').replace('</b>', '')}: Test could not be executed due to: {repr(e)}",
            ]
        )

    # duplicated item level images
    t_msg = f"Occurrences of <b>duplicated item level images</b>"
    try:
        i_df = pd.DataFrame(
            [item for sublist in df["image_urls"].values for item in sublist]
        )
        i_df[0] = i_df.apply(lambda x: re.sub(r"\?\S+=\S+", "", x[0]), axis=1)
        t_df = i_df[i_df[0].duplicated()]
        tests.append(process_df_test(t_df, t_msg, manual=True))
    except Exception as e:
        tests.append(
            [
                None,
                f"{t_msg.replace('<b>', '').replace('</b>', '')}: Test could not be executed due to: {repr(e)}",
            ]
        )

    # TODO: Test to check for at least 1 case of out of stock in product and variant level
    # TODO: Change _int tests to convert price fields from string
    #  to int and do the tests even not having _int fields as the previous notebook template prepared

    return tests
    
def get_last_jobs_from_project(project_id,excluding_filter=None):
    if "/" not in project_id[0]:
        project_list = project_id
        client = ScrapinghubClient()
        jobs = []
        output = []

        for project_id in project_list:
            project = client.get_project(project_id)
            spiders = [ sub['id'] for sub in project.spiders.list() ]
        
            print(f"Retrieving last jobs from project {project_id}...")
            for s in tqdm(spiders):
                job = []
                if excluding_filter is None or excluding_filter not in s:
                    job = list(project.spiders.get(s).jobs.iter(state=['finished']))
                if len(job)>0:
                    jobs.concat[(job[0].get('key'))]
                    output.concat[(str(project.spiders.get(s).name), ": " ,job[0].get('key'))]
                    
        print(f"Jobs retrieved with spider name are the following: \n{output}\n")
        return jobs
    else:
        jobs_from_input = project_id
        return jobs_from_input
    
def log_pqa_metric(input,feature_used,item_amount):
    import datetime;from pathlib import Path;import re,boto3;now=datetime.datetime.now();year=now.strftime('%Y');file=now.strftime('%W');path=f"reports/dqr/pqa_stats/{year}/";Path(path).mkdir(parents=True,exist_ok=True);filename=f"{path}{file}.csv";f=open(filename,'a+');timestamp=now.strftime('%d/%m/%Y %H:%M:%S');org_id='';jobs_amount=None
    if isinstance(input,list):org_id=input[0];jobs_amount=len(input)
    elif isinstance(input,dict):org_id=list(input)[0];jobs_amount=len(input)
    elif isinstance(input,str):org_id=input;jobs_amount=1
    org_id=re.search('^[0-9]+',org_id).group(0);f.write(f"{timestamp},{org_id},{feature_used},{jobs_amount},{item_amount}\n");f.close();boto3.client('s3').upload_file(filename,'files.scrapinghub.com',filename)
    
def search_aggregated_df(original_df, query, return_fields=None, sample_amount=3):
    fdf = pd.DataFrame()
    job_list = original_df[['_job']].drop_duplicates().values.tolist()
    flat_list = [item for sublist in job_list for item in sublist]
    item_amount = original_df[['_key']].shape[0]
    log_pqa_metric(flat_list,3,item_amount)
    for job in tqdm(original_df['_job'].dropna().unique()):
        d = original_df[original_df['_key'].str.contains(job)]
        d = d.query(query)
        try:
            fdf = pd.concat([d.sample(sample_amount)])
        except ValueError:
            if len(d.index) > 0:
                fdf = pd.concat([d.sample(len(d.index))])       
    if fdf.shape[0] == 0:
        prt = f"0 items found with given query: {query}"
        if return_fields:
            prt += f" and return_fields: {return_fields}"
        print(prt)
        return fdf
    else:
        if return_fields:
            display(HTML(fdf[return_fields].to_html()))
            return fdf[return_fields]
        else:
            display(HTML(fdf.to_html()))
        return fdf
        
def remove_inexistent_columns(df,columns):
    for column in columns:
        if column not in df.columns:
            columns.pop(column)
    
def test_search(original_df, query, test_title = None, return_fields = None):
    test_df = search_aggregated_df(original_df, query, return_fields)
    test = test_df.shape[0]==0
    if not test_title:
        test_title = "Test"
    if test:
        test_result = "<span style='color:green'>Passed</span>"
    else:
        test_result = "<span style='color:red'>Failed</span>"
    display(Markdown(f"### {test_title}. Result: {test_result}"))
    return test

def prepare_df_for_aggregated_search(df):
    df['_pos'] = df['_key'].str.rfind('/')
    df['_job'] = df.apply(lambda x: x['_key'][0:x['_pos']],axis=1)
    df = df.drop(columns=['_pos'])
    return df    
